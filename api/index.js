const express = require("express");
const api = express.Router();
const cors = require("cors");

// Middleware for handling multipart/form-data
const multer = require("multer");
// const upload = multer({ dest: 'uploads/' })

// Prevents "has been blocked by CORS policy" error
api.use(cors());

// SET STORAGE
// help from https://code.tutsplus.com/tutorials/file-upload-with-multer-in-node--cms-32088
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads");
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + "-" + Date.now());
  },
});

var upload = multer({ storage: storage });

// for parsing multipart/form-data
// api.use(upload.array());

// Root of this router should be '/api'
api.get("/", (req, res) => {
  return res.json({ message: "Request recieved." });
});

api.post("/", (req, res) => {
  console.log("Request parameters", req.params);
  console.log("Request body", req.body);
  res.json(req.body);
});

// Try out form data, uploads, and stuff here
const formUpload = upload.fields([
  { name: "AgencyLicenseFile", maxCount: 1 },
  { name: "ProofOfInsuranceFile", maxCount: 1 },
  { name: "W9File", maxCount: 1 },
]);
api.post("/multer", formUpload, (req, res, next) => {
  console.log("Request body", req.body);

  // Check if there are attached files
  const files = req.files;
  if (!files) {
    const error = new Error("Please choose files");
    error.httpStatusCode = 400;
    return next(error);
  }

  res.send("Multer says hi");
});

// This route will give a 400 status
api.get("/400", (req, res) => {
    res.status(400).send("You tried to GET a route that is no bueno; 400 status!");
});
api.post("/400", (req, res) => {
    res.status(400).send("You tried to POST a route that is no bueno; 400 status!");
});

module.exports = api;
