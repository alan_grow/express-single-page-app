Production Server for SPA
========================

This is a simple express server for serving up apps created with `create-react-app`.

This server is also set up to test things like file uploads.

## Usage

*Copy-pasta* the built directory of some Single Page App into the root of this
*directory. Rename it as `build/` if it is called something else (e.g. `dist/`
*for distribution).

To start the server, run `npm run start`.

## Alternatives

To more easily do something very similar to what this app is doing, you can just do

```bash
npm install -g serve
serve -s build
```

I learned of this from (official) output while using `create-react-app`.