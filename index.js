const express = require('express');
const path = require('path');
const app = express();
const api = require('./api/index');
const morgan = require('morgan');

const PORT = process.env.PORT || 8080;

// Use morgan for logging
app.use(morgan('combined'));

// middleware for handling JSON body
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// serve static assets normally
app.use(express.static(path.join(__dirname, 'build')));

// api route for trying out posting data, uploading files, etc. 
app.use('/api', api);

// handle every other route with index.html, which will contain
app.get('/*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'build', 'index.html'));
});

app.listen(PORT, () => {
    console.log("server started on port", PORT);
});
